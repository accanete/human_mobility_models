import networkx as nx
import numpy as np
import pickle
import matplotlib.pyplot as plt
import itertools


def getDs(g):
    '''    
    obtain distances between nodes
    '''
    d = []
    err = []
    combs = list(itertools.combinations(g.nodes(),2))
    for i in combs: 
        if i[1] in g[i[0]]:
            try:
                s = g.edge[i[0]][i[1]]['distance']
                d.append(s)
                if s == 0:
                    print i
            except:
                KeyError
                err.append([i])
    return d, err


def obtainBasicMetrics(network):
    '''
    Obtains basic network information from network to be analyzed
    '''

    degree = nx.degree(network)
    shortestPathEdges = nx.shortest_path_length(network)
    avgShortestPathEdges = nx.average_shortest_path_length(network)
    shortestPathDistances = nx.shortest_path_length(network,weight='distance')
    avgShortestPathDistances = nx.average_shortest_path_length(network,weight='distance')
    clustering = nx.clustering(network)
    density = nx.density(network)
    degCent = nx.degree_centrality(network)
    betCent = nx.betweenness_centrality(network)
    closeCent = nx.closeness_centrality(network)
    return degree,shortestPathEdges,avgShortestPathEdges,shortestPathDistances,avgShortestPathDistances,\
    clustering, density, degCent, betCent, closeCent

j = open('phgtfsNetwork.pkl','rb')
tNet = pickle.load(j)

#Obtaining Metric Values
degree, shortestPathEdges, avgSPE, shortestPathDistances, avgSPD, clustering, \
density, degCent, betCent, closeCent = obtainBasicMetrics(tNet)

edgeList = tNet.edges()
edgeWeights = [tNet.edge[i[0]][i[1]]['distance'] for i in edgeList]

nodes = tNet.nodes()
nodeCombs = list(itertools.combinations(nodes,2))
shortestDistancesPerPair = [shortestPathDistances[i[0]][i[1]] for i in nodeCombs]

fig = plt.figure(100)
plt.title('Shortest Distance',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(shortestDistancesPerPair,15)

#Graphing
fig0 = plt.figure(0)
plt.title('Degree',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(degree.values(),15)

fig1 = plt.figure(1)
plt.title('Shortest Path Edges',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(shortestPathEdges.values(),10)

fig2 = plt.figure(2)
plt.title('Shortest Path Distance',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(shortestPathDistances.values(),10)

fig3 = plt.figure(3)
plt.title('Clustering',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(clustering.values(),15)

fig3 = plt.figure(4)
plt.title('Betweenness Centrality',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(betCent.values(),15)

fig3 = plt.figure(5)
plt.title('Closeness Centrality',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(closeCent.values(),15)

heat = []
beat = []
for i in tNet.nodes():
    heat.append(tNet.node[i]['heat_weight'])
    beat.append(degCent[i])


d, err = getDs(tNet)
b = np.arange(0,2.5,0.1)
plt.xlabel('Distance',size=20)
plt.ylabel('Frequency',size=20)
plt.xticks(size=20)
plt.yticks(size=20)
plt.hist(d,bins=b)   