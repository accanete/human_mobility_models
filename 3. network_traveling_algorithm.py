import os
import networkx as nx
import pickle
import random
import numpy as np

def haversine(lon1,lat1,lon2,lat2,r):
    '''
    calculates the distance between two coordinates using the harversin formula, returns
    in km
    '''
    lon1,lat1,lon2,lat2 = np.radians([lon1,lat1,lon2,lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat/2)**2 + np.cos(lat1)*np.cos(lat2)*np.sin(dlon/2)**2
    d = 2*r*np.arcsin(np.sqrt(a))
    return d

def randPointFind(lon1,lat1,d,lonsMin,lonsMax,latsMin,latsMax):
    '''
    Finds a random point, a distance away
    '''
    angles = np.linspace(1,360,360)
    possiblePoints = []
    for i in angles:
        lon2 = lon1 + np.sin(i)*d 
        lat2 = lat1 + np.cos(i)*d
        possiblePoints.append([lon2,lat2])
    remove = []
    for i in possiblePoints:
        if i[0] < lonsMin or i[0] > lonsMax or i[1] < latsMin or i[1] > latsMax:
            remove.append(i)
    for i in remove:
        possiblePoints.remove(i)
    return random.choice(possiblePoints)
    

def randPointFindRadius(lon1,lat1,d,center,radius):
    '''
    Finds a random point, a distance away
    '''
    angles = np.linspace(1,360,360)
    possiblePoints = []
    for i in angles:
        lon2 = lon1 + np.sin(np.deg2rad(i))*d
        lat2 = lat1 + np.cos(np.deg2rad(i))*d
        possiblePoints.append([lon2,lat2])
    remove = []
    for i in possiblePoints:
        r = np.sqrt(np.square(i[0]-center[0]) + np.square(i[1]-center[1]))
        if r > radius:
            remove.append(i)
    for i in remove:
        possiblePoints.remove(i)
    return random.choice(possiblePoints)

def closestHub(tNetwork, point):
    '''
    finds closest hub to point
    '''
    r = 6373.0
    lon1,lat1 = point
    distD = {}
    for i in tNetwork.nodes():
        lon2,lat2 = tNetwork.node[i]['coords']
        distD[haversine(lon1,lat1,lon2,lat2,r)] = i
    dMin = min(distD.keys())
    return distD[dMin],dMin


def shortestPaths(tNetwork, rPoints, tNetwork2):
    '''
    returns augmented rPoints dictionary; adds walking distance + vehicular distance
    '''
    for i in rPoints.keys():
        point1 = rPoints[i]['pair'][0]
        point2 = rPoints[i]['pair'][1]
        hub1,walk1 = closestHub(tNetwork,point1)
        hub2,walk2 = closestHub(tNetwork,point2)
        nPath = nx.shortest_path(tNetwork,hub1,hub2,'distance')
        nLength = nx.shortest_path_length(tNetwork,hub1,hub2,'distance')
        print nLength
        interSubgraphWalk = nx.shortest_path_length(tNetwork2,hub1,hub2,'distance') # distance for walking between subgraphs
        rPoints[i]['nDistance'] = {'walk':walk1+walk2+interSubgraphWalk,\
                                   'vehicle':nLength+interSubgraphWalk,\
                                   'path':nPath}
    return rPoints



fileName = 'randomPointsCenterCircle.pkl'
fileNameSave = 'randomPointsCenterCircle+nDistance.pkl'

#os.chdir('C:\Users\Ariel\Work\CX\Thesis\Thesis Work in Progress\\repo\Philippines_gtfs')
s = open('phgtfsNetworkDIR.pkl','rb')
sH = open('phgtfsNetworkONLYINTERSUB.pkl','rb')
stopsG = pickle.load(s)
stopsGH = pickle.load(sH)
s.close()
sH.close()
p = open(fileName,'rb')
points = pickle.load(p)
p.close()

#save data
stopsG2 = shortestPaths(stopsG,points,stopsGH)
stopsG2save = open(fileNameSave,'wb')
pickle.dump(stopsG2,stopsG2save)
stopsG2save.close()


