import os
import matplotlib.pyplot as plt
import pickle
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from functions_network_epidemic import *
import itertools
from multiprocessing import Pool

currentDir = os.getcwd()
j = open(currentDir+'/randpoints+congestion.pkl')
tNetOrig = pickle.load(j)
j.close()

def mainFunction((infC,recC)):
    #create folders
    folder = str("network_vanilla_infC_" + str(infC) + "_recC_" + str(recC))       
    os.mkdir(folder)
    os.chdir(folder)
    #initialize values and data containers
    rho = {10:[],50:[],100:[]}    
    networks = []    
    time = 1500
    initialSeed = 0.0005    
    
    for i in range(1):
        #load network
        tNet = tNetOrig.copy()
        
        #set up infection
        maxNodes = len(tNet.nodes())
        tData = {'infectedAgent':{i:[] for i in xrange(time)},'S':{i:[] for i in xrange(time)},\
                'I':{i:[] for i in xrange(time)},'R':{i:[] for i in xrange(time)}}
        spreading_init(tNet)
        spreading_seed(tNet, initialSeed)
        update_data(tNet,tData,0)
        spreading_run(tNet, time, tData, infC, recC)
        plt.plot(range(len(tData['I'].values())),tData['I'].values()) #plotting
        
        #filtering 
        c = tData['I'].values()
        t = range(len(tData['I'].values()))
        spacing = 1000
        xx = np.linspace(min(t),max(t),spacing)
        itp = interp1d(t,c,bounds_error=False,fill_value=0., kind='linear')  #interpolate
        window_size, poly_order = 101,3                                      #use savitzky-golay filter
        yy_sg = savgol_filter(itp(xx),window_size, poly_order)    
        i10 = yy_sg[np.floor(spacing*0.1)]
        i50 = yy_sg[np.floor(spacing*0.5)]
        i100 = yy_sg[-1]
        rho10,rho50,rho100 = i10/maxNodes, i50/maxNodes, i100/maxNodes
        rho[10].append(rho10)
        rho[50].append(rho50)
        rho[100].append(rho100)
        networks.append(tNet)
        print rho10, rho50, rho100
    
    #save stuff
    m = open(str(infC) + '_' + str(recC) + '_rhoData.pkl' ,'wb')
    pickle.dump(rho,m)
    m.close()    
    n = open(str(infC) + '_' + str(recC) + '_nodesData.pkl' ,'wb')
    pickle.dump(tNet,n)
    n.close()   
    os.chdir(currentDir)
        
        
        
infC =np.linspace(0.001,0.5,10)
recC =np.linspace(0.001,0.5,10)
value = itertools.product(infC,recC)   

if __name__ == '__main__':             #FOR MULTIPROCESSING
    pool = Pool(1)                     #FOR MULTIPROCESSING
    pool.map(mainFunction,value)       #FOR MULTIPROCESSING