import fiona
import os
import matplotlib.pyplot as plt
import csv
import random
from shapely.geometry import Polygon, Point
import pickle
import numpy as np
import ast


cities = ['kalookan-bgy-population.shp','laspiñas-bgy-population.shp','makati-bgy-\
population.shp','malabon-bgy-population.shp','mandaluyong-bgy-\
population.shp','manila-bgy-population.shp','marikina-bgy-\
population.shp','muntinlupa-bgy-population.shp','navotas-bgy-\
population.shp','parañaque-bgy-population.shp','pasay-bgy-\
population.shp','pasig-bgy-population.shp','pateros-bgy-\
population.shp','quezoncity-bgy-population.shp','sanjuan-bgy-\
population.shp','taguig-bgy-population.shp','valenzuela-bgy-\
population.shp']

businessDistricts = {'Ortigas Center':[121.059916, 14.584473],\
'Makati BD':[121.029116, 14.555607],\
'The Fort':[121.051286, 14.550588],\
'Alabang':[121.04200,  14.416917],\
'Triangle Park':[121.02367,  14.557093]}
        

###################################################################################### FUNCTIONS FROM LevyFlightGTFS.py

def haversine(lon1,lat1,lon2,lat2,r):
    '''
    calculates the distance between two coordinates using the harversin formula, returns
    in km
    '''
    lon1,lat1,lon2,lat2 = np.radians([lon1,lat1,lon2,lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat/2)**2 + np.cos(lat1)*np.cos(lat2)*np.sin(dlon/2)**2
    d = 2*r*np.arcsin(np.sqrt(a))
    return d
   

def randPointWithinPoly(lon1,lat1,d,poly):
    '''
    Finds a random point using a radius. Augmented with the polygon boundaries
    '''
    angles = np.linspace(1,360,360)
    possiblePoints = []
    for i in angles:
        lon2 = lon1 + np.sin(np.deg2rad(i))*d
        lat2 = lat1 + np.cos(np.deg2rad(i))*d
        possiblePoints.append([lon2,lat2])
    remove = []
    for i in possiblePoints:
        p = Point(lon2,lat2)
        if poly.contains(p) == False:
            remove.append(i)
    for i in remove:
        possiblePoints.remove(i)
    return random.choice(possiblePoints)

def randPointFindRadius(lon1,lat1,d,center,radius):
    '''
    Finds a random point, a distance away at a random angle
    '''
    angles = np.linspace(1,360,360)
    possiblePoints = []
    for i in angles:
        lon2 = lon1 + np.sin(np.deg2rad(i))*d
        lat2 = lat1 + np.cos(np.deg2rad(i))*d
        possiblePoints.append([lon2,lat2])
    remove = []
    for i in possiblePoints:
        r = np.sqrt(np.square(i[0]-center[0]) + np.square(i[1]-center[1]))
        if r > radius:
            remove.append(i)
    for i in remove:
        possiblePoints.remove(i)
    return random.choice(possiblePoints)
    

######################################################## OBTAIN OUTLINE OF METRO MANILA

shp = fiona.open('regn-population.shp')
geoCoords = shp[-1]['geometry']['coordinates'][0][0]

xmm, ymm,coords = [],[],[]

for i in geoCoords:
    xmm.append(i[0])
    ymm.append(i[1])
    coords.append(i)

metrom = Polygon(coords)

######################################################## BUSINESS DISTRICT DISTRIBUTION

randPoints = {}
center = businessDistricts[businessDistricts.keys()[0]] 
r = 0.04 #~4.3 km
angles = np.linspace(1,360,360)
counter  = 0
for i in businessDistricts.keys():
    center = businessDistricts[i] 
    for j in range(1000):
        point1 = []
        point2 = []
        r_new = np.abs(np.random.normal()*r/3.0)
        possiblePoints = []
        for k in angles:
            lon1 = center[0] + np.sin(np.deg2rad(k))*r_new 
            lat1 = center[1] + np.cos(np.deg2rad(k))*r_new
            p = Point(lon1, lat1)
            if metrom.contains(p) == True:
                possiblePoints.append([lon1,lat1])
        lon1, lat1 = random.choice(possiblePoints)
        point1.append(lon1)
        point1.append(lat1)
        
        r_new = np.abs(np.random.normal()*r/3.0)
        for k in angles:
            lon2 = center[0] + np.sin(np.deg2rad(k))*r_new 
            lat2 = center[1] + np.cos(np.deg2rad(k))*r_new
            p = Point(lon2, lat2)
            if metrom.contains(p) == True:
                possiblePoints.append([lon2,lat2])
        lon2, lat2 = random.choice(possiblePoints)
        point2.append(lon2)
        point2.append(lat2)
        print point1, point2
        randPoints[counter] = {'pair':[point1,point2],'hDistance':haversine(point1[0],point1[1],point2[0],point2[1],6373.0)}
        counter += 1
        print counter

######################################################## OBTAIN POPULATION DISTRIBUTION BY BARANGAY

os.chdir('C:\Users\Ariel\Work\CX\Thesis\Population Density Data\ProvincialDataSets\Population-by-bgy')
citsDict = {}
for i in cities:
    f = fiona.open(i.decode('UTF-8'))
    citsDict[i] = len(f)

bgys = {}
count = 0
totalProb = 0
bgyPoints = []
bgyProbs  = []
metromanPopulation = 9906048.0
for i in cities:
    city = fiona.open(i.decode('UTF-8'))
    for i in range(len(city)):
        coords = city[i]['geometry']['coordinates'][0] 
        while len(coords) < 2: 
            coords = coords[0]
        poly = Polygon(coords)
        cPoint = poly.centroid
        cPointcoords = [cPoint.x,cPoint.y]
        if city[i]['properties']['POP2K'] != None:
            population = city[i]['properties']['POP2K']
            prob = population/metromanPopulation
            totalProb += prob
            bgyProbs.append(prob)
            bgyPoints.append(str(cPointcoords))
            bgys[city[i]['properties']['NAMEJN2002']] = {'cPoint':cPointcoords,'population':population,'probability':prob}
            count += 1

counter = 0
randPoints = {}
for i in range(5000):
    point1 = ast.literal_eval(np.random.choice(bgyPoints,p=bgyProbs))
    point2 = ast.literal_eval(np.random.choice(bgyPoints,p=bgyProbs))
    randPoints[counter] = {'pair':[point1,point2],'hDistance':haversine(point1[0],point1[1],point2[0],point2[1],6373.0)}
    counter += 1
    print counter

c = open('barangayODPoints.pkl','wb')
pickle.dump(randPoints,c)
c.close()



######################################################## GENERATE RANDOM POINTS WITHIN METRO MANILA REGION


## generate 5000 random point pairs
counter  = 0
randPoints = {}
for i in range(5000):
    point1 = []
    point2 = []
    while True:
        lon1 = np.random.uniform(x1,x2)
        lat1 = np.random.uniform(y1,y2)
        p1 = Point(lon1,lat1)
        if metrom.contains(p1) == True:
            break     
    point1.append(lon1)
    point1.append(lat1)
    while True:
        lon2 = np.random.uniform(x1,x2)
        lat2 = np.random.uniform(y1,y2)
        p1 = Point(lon2,lat2)
        if metrom.contains(p1) == True:
            break      
    point2.append(lon2)
    point2.append(lat2)
    print point1, point2
    randPoints[counter] = {'pair':[point1,point2],'hDistance':haversine(point1[0],point1[1],point2[0],point2[1],6373.0)}
    counter += 1
    print counter

c = open('withinMetroManilaODPoints.pkl','wb')
pickle.dump(randPoints,c)
c.close()

xmm = []
ymm = []
for i in randPoints.keys():
	xmm.append(randPoints[i]['pair'][0][0])
	ymm.append(randPoints[i]['pair'][0][1])
	xmm.append(randPoints[i]['pair'][1][0])
	ymm.append(randPoints[i]['pair'][1][1])

for i in range(len(xmm)):
    plt.scatter(xmm[i],ymm[i])
    
######################################################## GENERATE RANDOM POINTS CENTERED CIRCULAR
    
randPoints = {}
center = [121.0581, 14.6058]
r = 0.0854
angles = np.linspace(1,360,360)
counter  = 0
for j in range(5000):
    point1 = []
    point2 = []
    r_new = np.abs(np.random.normal()*r/3.0)
    possiblePoints = []
    for k in angles:
        lon1 = center[0] + np.sin(np.deg2rad(k))*r_new 
        lat1 = center[1] + np.cos(np.deg2rad(k))*r_new
        possiblePoints.append([lon1,lat1])
    lon1, lat1 = random.choice(possiblePoints)
    point1.append(lon1)
    point1.append(lat1)
    print i, center, r_new
    r_new = np.abs(np.random.normal()*r/3.0)
    possiblePoints = []
    for k in angles:
        lon2 = center[0] + np.sin(np.deg2rad(k))*r_new 
        lat2 = center[1] + np.cos(np.deg2rad(k))*r_new
        possiblePoints.append([lon2,lat2])
    lon2, lat2 = random.choice(possiblePoints)
    point2.append(lon2)
    point2.append(lat2)
    print point1, point2
    randPoints[counter] = {'pair':[point1,point2],'hDistance':haversine(point1[0],point1[1],point2[0],point2[1],6373.0)}
    counter += 1
    print counter    
         
######################################################## PLOT TRANSIT POINTS
    
os.chdir("C:\Users\Ariel\Work\CX\Thesis\Thesis Work in Progress\\repo\Philippines_gtfs")
open_stops = open('stop_times.csv')
open_route_intersections = open('stops.csv')
stop_times = csv.reader(open_stops)
stop_weights = csv.reader(open_route_intersections)

##list points
stops_dict = {}
next(stop_weights)
for row in stop_weights:
    try:
        x = float(row[5])
    except:
        ValueError
    try:
        y = float(row[4])
    except:
        ValueError
    stops_dict[row[0]] = {'coords':[x,y],'links':[]}

