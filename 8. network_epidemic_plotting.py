import glob
import os
import matplotlib.pyplot as plt
import numpy as np
import pickle
import seaborn

top = os.getcwd()
dirList = glob.glob('network_*')

rhoMeansD = {(float('%.5f'%float(i.split('_')[3])),float('%.5f'%float(i.split('_')[5]))):{10:0,50:0,100:0} for i in glob.glob('network_*')}
rhoSTDD = {(float('%.5f'%float(i.split('_')[3])),float('%.5f'%float(i.split('_')[5]))):{10:0,50:0,100:0} for i in glob.glob('network_*')}
nodeData = {(float('%.5f'%float(i.split('_')[3])),float('%.5f'%float(i.split('_')[5]))):{'NodeDegree':[],'NodeInf%':[],'NodeHeat':[]} for i in glob.glob('network_*')}

values = np.linspace(0.001,0.5,10)
values = [float('%.5f'%(i)) for i in values]
[0.001, 0.05644, 0.11189, 0.16733, 0.22278, 0.27822, 0.33367, 0.38911, 0.44456, 0.5]

for i in dirList:
    os.chdir(i)
    n = glob.glob('*nodesData.pkl')
    r = glob.glob('*rhoData.pkl')
    ns = open(n[0],'rb')
    rs = open(r[0],'rb')
    nData = pickle.load(ns)
    rData = pickle.load(rs)
    ns.close()
    rs.close()
    
    infC = float('%.5f'%float(n[0].split('_')[0]))
    recC = float('%.5f'%float(n[0].split('_')[1]))
    rhoMeansD[(infC,recC)][10]  = np.mean(rData[10])
    rhoMeansD[(infC,recC)][50]  = np.mean(rData[50])
    rhoMeansD[(infC,recC)][100] = np.mean(rData[100])
    rhoSTDD[(infC,recC)][10]  = np.std(rData[10]) 
    rhoSTDD[(infC,recC)][50]  = np.std(rData[50])
    rhoSTDD[(infC,recC)][100] = np.std(rData[100])
    for i in nData.nodes():
        nodeData[(infC,recC)]['NodeDegree'].append(len(nData[i].keys()))
        nodeData[(infC,recC)]['NodeInf%'].append(nData.node[i]['timeInf'])
        nodeData[(infC,recC)]['NodeHeat'].append(nData.node[i]['heat_weight'])
    os.chdir(top)    

#corrData = {}
corrData1 = {}
corrData2 = {}
scores1 = []
scores2 = []        

for j in corrData1:
    scores1.append(corrData1[j])

for j in corrData2:
    scores2.append(corrData2[j])    
    

#for plotting
timeToPlot = 100
nodeDataToPlot = ['NodeInf%', 'NodeHeat', 'NodeDegree']
plotData = []
for i in values:
    for j in values:
        plotData.append(rhoMeansD[(i,j)][timeToPlot])

p = np.reshape(plotData, (10,10))
fig, ax = plt.subplots()
heatmap = ax.pcolor(p)

plt.colorbar(heatmap)

ax.set_xticklabels([0.001,0.5],size = '10')
ax.set_yticklabels([0.001,0.5],size = '10')
ax.set_xticks([0,1])
ax.set_yticks([0,1])
plt.xticks(range(len(values)), [str(i) for i in values])
plt.yticks(range(len(values)), [str(i) for i in values])
ax.get_xaxis().majorTicks[1].label1.set_horizontalalignment('center')
plt.title('Mean At T = ' + str(timeToPlot),size='40')
plt.title('STD At T = ' + str(timeToPlot))
ax.set_xlabel('Recovery Chance',size='40')
ax.set_ylabel('Infection Chance',size='40')
plt.show()
plt.savefig('Mean At T = ' + str(timeToPlot) + '.png')
