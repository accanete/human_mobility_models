import os
import csv
import pickle
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import itertools

def haversine(lon1,lat1,lon2,lat2,r):
    '''
    calculates the distance between two coordinates using the harversine formula
    '''
    lon1,lat1,lon2,lat2 = np.radians([lon1,lat1,lon2,lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat/2)**2 + np.cos(lat1)*np.cos(lat2)*np.sin(dlon/2)**2
    d = 2*r*np.arcsin(np.sqrt(a))
    return d
   
def tripOrder(trips_list,stops_dict):
    '''
    order the trip list
    '''
    for i in trips_list.keys():
        route = trips_list[i]
        routeLen = len(route)
        if route.keys()[-1] != routeLen:
            replaceD = {}
            for j in range(routeLen):
                replaceD[j+1] = trips_list[i].values()[j]
            route = replaceD        
        for j in route.keys():
            if j == 1:
                if route[2] not in stops_dict[route[j]]['links']:
                    stops_dict[route[j]]['links'].append(route[2])
            elif j == routeLen:
                if route[j-1] not in stops_dict[route[j]]['links']:
                    stops_dict[route[j]]['links'].append(route[j-1])
            else:
                stops_dict[route[j]]['links'].append(route[j-1])
                stops_dict[route[j]]['links'].append(route[j+1])
    return stops_dict
    
def completeGraph(nGraph,nGraph2):
    '''
    completes a graph by connecting closest nodes between every subgraph.
    nGraph2 is a replica of nGraph where only the newly established h-distance 
    links are counted
    '''    
    connectInfo = []       
    graphsDict = {}         
    connComp = nx.connected_components(nGraph) 
    count = 0
    for i in connComp:
        graphsDict[count] = i
        count += 1
    combs = list(itertools.combinations(graphsDict.keys(),2))
    for i,j in combs: 
        disDict = {}
        for k in graphsDict[i]:
            for l in graphsDict[j]:
                r = 6373.0
                lon1,lat1 = nGraph.node[k]['coords']
                lon2,lat2 = nGraph.node[l]['coords']
                disDict[haversine(lon1,lat1,lon2,lat2,r)] = [k,l]
        shortestCDist = min(disDict.keys())
        shortestCNodes  = disDict[shortestCDist]
        connectInfo.append([[str(i) + '-' + str(j)],shortestCNodes,shortestCDist])
        nGraph.add_edge(shortestCNodes[0],shortestCNodes[1],distance=shortestCDist)
        nGraph2.add_edge(shortestCNodes[0],shortestCNodes[1],distance=shortestCDist)
    return nGraph, connectInfo, nGraph2

def graphPointsP(nGraph):
    '''
    graph the nodes with lines connecting nodes
    '''
    for i in nGraph.nodes():
        for j in nGraph[i].keys():
            plt.plot([nGraph.node[i]['coords'][0],nGraph.node[j]['coords'][0]],\
                [nGraph.node[i]['coords'][1],nGraph.node[j]['coords'][1]])
    
def graphPointsS(nGraph):
    '''
    graph the points in a simple scatter format
    '''
    for i in nGraph.nodes():
        plt.scatter(nGraph.node[i]['coords'][0],nGraph.node[i]['coords'][1])

open_stops = open('stop_times.csv')
open_route_intersections = open('stops.csv')
stop_times = csv.reader(open_stops)
stop_weights = csv.reader(open_route_intersections)

#list points
next(stop_weights)
stop_weights_list = []
for row in stop_weights:
    stop_weights_list.append(row)

stops_dict = {}
for row in stop_weights_list:
    try:
        x = float(row[5])
    except:
        ValueError
    try:
        y = float(row[4])
    except:
        ValueError
    stops_dict[row[0]] = {'coords':[x,y],'links':[]}

##list trips FOR THE PHILIPPINES
next(stop_times)
stop_times_list = []
for row in stop_times:
    stop_times_list.append(row)

t_list = {}
for row in stop_times_list:
    x = int(row[1])
    y = row[2]
    if t_list.has_key(row[0]) == True:
        t_list[row[0]][x] = y
    else:
        t_list[row[0]] = {x:y}
 
stops_dict = tripOrder(t_list,stops_dict)

#just a list of coordinates of stops
coords = []
for i in stops_dict.keys():
    coords.append(stops_dict[i]['coords'])


#build the network graph
    
stopsG = nx.Graph()    
for i in stops_dict.keys():
    stopsG.add_node(i,coords=stops_dict[i]['coords'])
    
stopsG2 = stopsG.copy() 

r = 6373.0
for i in stopsG.nodes():
    for j in stops_dict[i]['links']:
        lon1,lat1 = stopsG.node[i]['coords']
        lon2,lat2 = stopsG.node[j]['coords']
        stopsG.add_edge(i,j,distance = haversine(lon1,lat1,lon2,lat2,r))
        stopsG2.add_edge(i,j,distance=0)  

if nx.is_connected(stopsG) != True:
    stopsG,connectInfo,stopsG2 = completeGraph(stopsG,stopsG2)


fileSave1 = open('phgtfsNetwork.pkl','wb')
pickle.dump(stopsG,fileSave1)




