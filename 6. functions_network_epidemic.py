import matplotlib.pyplot as plt
import pickle
import numpy.random as rnd

SPREADING_SUSCEPTIBLE = 'S'
SPREADING_INFECTED = 'I'
SPREADING_RECOVERED = 'R'

j = open('randomPoints+LFDictBALANCED+HEAT.pkl')
tNet = pickle.load(j)
j.close()

def spreading_init(g):
    """Initialise all node in the graph to be susceptible."""
    for i in g.node.keys():
        g.node[i]['state'] = SPREADING_SUSCEPTIBLE
        g.node[i]['timeInf'] = 0

def spreading_seed( g, pSick ):
    """Infect nodes with high heat values in the graph."""
    for i in g.node.keys():
        if(rnd.random() <= pSick):
            g.node[i]['state'] = SPREADING_INFECTED

def spreading_seedOrig( g, pSick ):
    """Infect a random proportion of nodes in the graph."""
    for i in g.node.keys():
        if(rnd.random() <= pSick):
            g.node[i]['state'] = SPREADING_INFECTED
            
def update_data(g, data, t):
    '''
    update the time data per step
    '''
    S = 0
    I = 0
    R = 0
    for i in g.nodes():
        if g.node[i]['state'] == 'S':
            S += 1
        elif g.node[i]['state'] == 'I':
            I += 1
            data['infectedAgent'][t].append(i)
        elif g.node[i]['state'] == 'R':
            R += 1
    data['S'][t] = S
    data['I'][t] = I
    data['R'][t] = R           

def modelRun( g, pInfect, pRecover):
    toInfect = []
    for i in g.nodes():
        if g.node[i]['state'] == SPREADING_INFECTED:
            g.node[i]['timeInf'] += 1
            # infect susceptible neighbours with probability pInfect
            for j in g.neighbors(i):
                if g.node[j]['state'] == SPREADING_SUSCEPTIBLE:
                    if rnd.random() <= pInfect:
                        toInfect.append(j)
            # recover with probability pRecover
            if rnd.random() <= pRecover:
#                g.node[i]['state'] = SPREADING_RECOVERED
                g.node[i]['state'] = SPREADING_SUSCEPTIBLE
#    print toInfect
    for i in toInfect:
        g.node[i]['state'] = SPREADING_INFECTED
         
def spreading_step( g, gdata, t, infC, recC):
    """Run a single step of the model over the graph."""
    modelRun(g, infC, recC)
    update_data(g, gdata,t)

def spreading_run( g, runs, gdata, infC, recC):
    """Run a number of iterations of the model over the graph."""
    for i in xrange(1,runs):
        spreading_step(g, gdata, i, infC, recC)

def obtainIData(data):
    I = data['I']
    iMax = max(I.values())
    tMax = I.values().index(iMax)
    return iMax, tMax


def plotNetwork(g):
    for i in g.nodes():
        if tNet.node[i]['state'] == 'S':
            c = 'b'
        elif tNet.node[i]['state'] == 'I':
            c = 'r'
        elif tNet.node[i]['state'] == 'R':
            c = 'g'
        coords = [ tNet.node[i]['coords'][0], tNet.node[i]['coords'][1]]
        plt.scatter(coords[0],coords[1],color=c)