import os
import numpy as np
import pickle
import matplotlib.pyplot as plt


os.chdir('C:\Users\Ariel\Work\CX\Thesis\Thesis Work in Progress\\repo\data_safe_keeping')

G       = open("phgtfsNetwork.pkl",'rb')
network = pickle.load(G)
G.close()

tPoints = []
for i in network.nodes():
    tPoints.append(i)

## By barangay
#fDict = open('DICTbarangayODPoints+nDistance.pkl')
#fCong = open('barangay+nDistance+congestion.pkl')

## By business district
#fDict = open('DICTbdODPoints+nDistance.pkl')
#fCong = open('bdODPoints+nDistance+congestion.pkl')

## Within Metro Manila 
#fDict = open('DICTwithinMetroManilaODPoints+nDistance.pkl')
#fCong = open('withinMetroManila+nDistance+congestion.pkl')
#
# Within center gaussian
fDict = open('randomPoints+LFDictBalancedCenterCIRCLE.pkl')
fCong = open('randomPoints+LFDictBalancedCenterCIRCLE+HEAT.pkl')


pairDict = pickle.load(fDict)
congMap  = pickle.load(fCong)

fDict.close()
fCong.close()

nodeCong = {}
for i in pairDict.keys():
    for j in pairDict[i]['nDistance']['path']:
        if nodeCong.has_key(j) == False:
            nodeCong[j] = congMap.node[j]['heat_weight']

congVals = nodeCong.values()
nodeNo = len(congVals)
x = np.linspace(1,840,840)
xnew = [i/5000 for i in x]
y = []

for i in x:
    count = 0.
    for j in congVals:
        if j > i:
            count += 1
    y.append(count/nodeNo)

ylog = [np.log(i) for i in y]

plt.xlabel('Congestion threshold K' ,size='20')
plt.ylabel('Log fraction of area congested',size='20')
plt.ylim(-9,0)
plt.xlim(0,0.18)
plt.plot(xnew,ylog,label='Centered Gaussian Area')
plt.legend()





